package com.yan.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单
 */

@Data
public class SysMenu implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "前端路由")
    private String path;
    @ApiModelProperty(value = "菜单图标")
    private String icon;
    private String title;
    @ApiModelProperty(value = "前端组件")
    private String component;

    @ApiModelProperty(value = "显示状态(0 不显示，1显示)")
    private boolean status;

    @ApiModelProperty(value = "父级菜单")
//    @JsonIgnore
    private Long parentId;

    @ApiModelProperty(value = "主菜单")
    private List<SysMenu> children;

}
