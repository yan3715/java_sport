package com.yan.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yan.mapper.SysMenuMapper;
import com.yan.mapper.SysPermissionMapper;
import com.yan.mapper.SysRoleMapper;
import com.yan.pojo.SysMenu;
import com.yan.pojo.SysPermission;
import com.yan.pojo.SysRole;
import com.yan.service.SysRoleService;
import com.yan.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private SysPermissionMapper permissionMapper;

    @Autowired
    private SysRoleMapper roleMapper;


    @Autowired
    private RedisUtils redisUtils;

    /**
     * 分页查询
     * @param queryInfo
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页-->页码{}-->页数{} --> 查询内容{}",queryInfo.getPageNumber(),queryInfo.getPageSize(),queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(),queryInfo.getPageSize());
        log.info("查询-->{}",queryInfo.getQueryString());
        Page<SysRole> page = roleMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数--> {}",total);
        List<SysRole> result = page.getResult();
        log.info("分页列表-->{}",result);

        result.forEach(item -> {
            // 查询角色下面的菜单信息
            List<SysMenu> menus =  menuMapper.findByRoleId(item.getId());
            menus.forEach(menu -> {
                // 查询出子级菜单
                List<SysMenu> children = menuMapper.findByRoleIdAndParentId(menu.getId(), item.getId());
                menu.setChildren(children);
            });
            // 封装角色菜单信息
            item.setMenus(menus);
            // 查询该角色权限信息
            List<SysPermission> permissions =  permissionMapper.findByRoleId(item.getId());
            item.setPermissions(permissions);
        });

        return new PageResult(total,result);
    }

    // 事务注解
    @Transactional
    @Override
    public Result insert(SysRole role) {
        // 健壮性判断
        log.info("查询角色信息是否存在");
        SysRole role1 =  roleMapper.findByLabel(role.getLabel());
        if(null != role1){
            return Result.fail("该角色已经存在");
        }
        // 插入角色信息 对于权限数据和菜单信息，我们可以先删除后添加这种形式来实现
        roleMapper.insert(role);
        if(role.getPermissions().size() > 0){
            log.info("在添加对应的权限数据");
            role.getPermissions().forEach(item -> {
                roleMapper.insertPermissions(role.getId(),item.getId());
            });


        }
        if(role.getMenus().size() > 0){
            log.info("在添加对应的菜单信息");

            role.getMenus().forEach(item -> {
                roleMapper.insertMenus(role.getId(),item.getId());
            });
        }
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("添加角色信息成功");
    }

    @Override
    public Result delete(Long id) {
        log.info("查询该角色信息下是否有菜单权限");
        List<SysMenu> menus = menuMapper.findByRoleId(id);
        List<SysMenu> childrens = new ArrayList<>();
        menus.forEach(item -> {
            List<SysMenu> childrenMenus = menuMapper.findByRoleIdAndParentId(item.getId(), id);
            childrens.addAll(childrenMenus);
        });
        if(menus.size() > 0 || childrens.size() > 0){
            return Result.fail("删除失败，该角色下拥有菜单信息,请先删除对应的菜单信息");
        }

        if(permissionMapper.findByRoleId(id).size() > 0) {
            return Result.fail("删除失败，该角色下拥有权限信息，请先删除对应的权限信息");
        }

        // 删除操作
        roleMapper.delete(id);
        return Result.success("删除成功");
    }

    @Transactional
    @Override
    public Result update(SysRole role) {
        roleMapper.update(role);
        if (role.getPermissions().size() > 0) {
            log.info("先删除对应的权限数据");
            roleMapper.deletePermissionByRoleId(role.getId());

            log.info("在添加对应的权限数据");
            role.getPermissions().forEach(item -> {
                roleMapper.insertPermissions(role.getId(),item.getId());
            });


        }
        if (role.getMenus().size() > 0) {
            log.info("添加菜单信息");
            roleMapper.deleteMenuByRoleId(role.getId());

            log.info("在添加对应的菜单信息");
            role.getMenus().forEach(item -> {
                roleMapper.insertMenus(role.getId(),item.getId());
            });
        }
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("修改角色信息成功");
    }

    @Override
    public Result findAll() {
        return Result.success("查询所有角色成功",roleMapper.findAll());
    }
}
