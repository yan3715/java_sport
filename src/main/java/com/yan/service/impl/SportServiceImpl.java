package com.yan.service.impl;

import com.yan.mapper.SportMapper;
import com.yan.pojo.Sport;
import com.yan.service.SportService;
import com.yan.utils.*;
import lombok.extern.slf4j.Slf4j;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



@Slf4j
@Service
public class SportServiceImpl implements SportService {

    @Autowired
    private SportMapper sportMapper;

    @Override
    public Result delete(Long id) {
        if (id == null) {
            return Result.fail("请传递删除ID");
        }
        sportMapper.delete(id);
        return Result.success("删除成功");
    }

    @Override
    public Result update(Sport sport) {
        sport.setUpdateName(SecurityUtils.getUsername());
        sport.setUpdateTime(DateUtils.getDateTime());
        sportMapper.update(sport);
        return Result.success("修改成功");
    }

    @Override
    public Result insert(Sport sport) {
        sport.setCreateName(SecurityUtils.getUsername());
        sport.setCreateTime(DateUtils.getDateTime());
        sportMapper.insert(sport);
        return Result.success("添加成功");
    }

    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始数据分页-->页码{}, --->{}页数--->查询内容{}", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<Sport> sports = sportMapper.findPage(queryInfo.getQueryString());
        long total = sports.getTotal();
        List<Sport> result = sports.getResult();
        log.info("查询的总条数-->{}", total);
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    @Override
    public Result findInfo(Long id) {
        return Result.success("查询成功", sportMapper.findInfo(id));
    }

}
