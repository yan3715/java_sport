package com.yan.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yan.mapper.SysMenuMapper;
import com.yan.pojo.SysMenu;
import com.yan.service.SysMenuService;
import com.yan.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private RedisUtils redisUtils;



    /**
     * 分页查询
     *
     * @param queryInfo 页码，页数大小，查询内容
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页-->页码{}-->页数{} --> 查询内容{}",queryInfo.getPageNumber(),queryInfo.getPageSize(),queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(),queryInfo.getPageSize());
        Page<SysMenu> page = menuMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        List<SysMenu> result = page.getResult();
        log.info("查询的总条数--> {}",total);
        log.info("分页列表-->{}",result);

        return new PageResult(total,result);
    }

    /**
     * 添加菜单数据
     *
     * @param menu
     * @return
     */
    @Override
    public Result insert(SysMenu menu) {
        menuMapper.insert(menu);
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("添加权限数据成功");
    }

    /**
     * 删除菜单数据
     *
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {

        menuMapper.delete(id);
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("删除菜单数据成功");

    }

    /**
     * 修改菜单数据
     *
     * @param menu
     * @return
     */
    @Override
    public Result update(SysMenu menu) {
        menuMapper.update(menu);
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("修改菜单数据成功");
    }

    /**
     * 查询所有父级菜单
     *
     * @return
     */
    @Override
    public Result findParent() {
        return Result.success("查询父级菜单成功成功",menuMapper.findParent());
    }
}
