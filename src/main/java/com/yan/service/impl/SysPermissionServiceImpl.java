package com.yan.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yan.mapper.SysPermissionMapper;
import com.yan.pojo.SysPermission;
import com.yan.service.SysPermissionService;
import com.yan.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 权限数据业务逻辑层
 */
@Service
@Slf4j
public class SysPermissionServiceImpl implements SysPermissionService {


    @Autowired
    private SysPermissionMapper permissionMapper;


    @Autowired
    private RedisUtils redisUtils;


    /**
     * 分页查询
     *
     * @param queryInfo 页码，页数大小，查询内容
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页-->页码{}-->页数{} --> 查询内容{}",queryInfo.getPageNumber(),queryInfo.getPageSize(),queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(),queryInfo.getPageSize());
        Page<SysPermission> page = permissionMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        List<SysPermission> result = page.getResult();
        log.info("查询的总条数--> {}",total);
        log.info("分页列表-->{}",result);

        return new PageResult(total,result);
    }

    /**
     * 添加权限数据
     *
     * @param sysPermission
     * @return
     */
    @Override
    public Result insert(SysPermission sysPermission) {
        permissionMapper.insert(sysPermission);
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("添加权限数据成功");
    }

    /**
     * 删除权限数据
     *
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {

        permissionMapper.delete(id);
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("删除权限数据成功");

    }

    /**
     * 修改权限数据
     *
     * @param sysPermission
     * @return
     */
    @Override
    public Result update(SysPermission sysPermission) {
        permissionMapper.update(sysPermission);
        redisUtils.delKey("userInfo_"+ SecurityUtils.getUsername());
        return Result.success("修改权限数据成功");
    }

    /**
     * 查询所有的权限信息
     *
     * @return
     */
    @Override
    public Result findAll() {
        return Result.success("查询权限信息成功",permissionMapper.findAll());
    }
}
