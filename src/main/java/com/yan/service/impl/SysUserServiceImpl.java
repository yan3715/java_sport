package com.yan.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yan.config.security.service.UserDetailServiceImpl;
import com.yan.mapper.SysUserMapper;
import com.yan.pojo.SysRole;
import com.yan.pojo.SysUser;
import com.yan.service.SysUserService;
import com.yan.utils.*;
import com.yan.vo.LoginVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private TokenUtils tokenUtils;

    @Resource
    private SysUserMapper sysUserMapper;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

//    @Autowired
//    private MyPasswordEncoderConfig myPasswordEncoderConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 查找所有的用户信息
     *
     * @return
     */
/*    @Override
    public Result findAll() {
        log.info("获取用户信息");
        List<SysUser> all = sysUserMapper.findAll();
        return Result.success("获取用户信息成功",all);
    }*/

    /**
     * 登录接口
     *
     * @param loginVo 登录参数  账户和密码
     * @return token 用token获取资源
     */
    @Override
    public Result login(LoginVo loginVo) {
        UserDetails userDetails = null;
        if("2".equals(loginVo.getType())) {
            if (!StringUtils.isNotEmpty(loginVo.getPhoneNumber()) || !StringUtils.isNotEmpty(loginVo.getCode())){
                return Result.fail("请填写完整信息");
            }
            // 验证码对比
            Object code = redisUtils.getValue(loginVo.getPhoneNumber() + "sms");
            if (code == null){
                return Result.fail("验证码已过期");
            }
            if (!code.toString().equals(loginVo.getCode())){
                return Result.fail("验证码不正确!");
            }
            userDetails = userDetailsService.loadUserByUsername(loginVo.getPhoneNumber());
        }else {
            if (!StringUtils.isNotEmpty(loginVo.getUsername()) || !StringUtils.isNotEmpty(loginVo.getPassword())){
                return Result.fail("请填写完整的信息!");
            }
            log.info("1. 开始登录");
            userDetails = userDetailsService.loadUserByUsername(loginVo.getUsername());
            log.info("2. 判断用户是否存在");
            if(null == userDetails || !passwordEncoder.matches(MD5Utils.md5(loginVo.getPassword()),userDetails.getPassword())){
                return Result.fail("账号或密码错误，请重新登录");
            }
        }
        // 账号 里面 1 表示没有被禁用，0表示被禁用  1 为true ，0为false
        if(!userDetails.isEnabled()){
            return Result.fail("该账号已禁用，请联系管理员");
        }
        log.info("登录成功");
        // 用来设置登录信息的token
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
        // 在SpringSecurity中保存登录的 在SpringSecurity中，只要获取到Authentication对象，就可以获取到登录用户的详细信息
        // 设置 Authentication 对象
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        log.info("根据登录信息获取token");
        // 借助JWT生成token
        //需要借助jwt来生成token
        String token = tokenUtils.generateToken(userDetails);
//        String token = "";
//        String token = tokenUtil.generateToken(userDetails);

        Map<String,String> map = new HashMap<>(2);
        map.put("tokenHead",tokenHead);
        map.put("token",token);
        return Result.success("登录成功",map);
    }

    /**
     * 根据用户名获取用户信息
     *
     * @param username
     * @return
     */
    @Override
    public SysUser findByUsername(String username) {
        return sysUserMapper.findByUsername(username);
    }

    /**
     * 分页查询 用户信息
     *
     * @param queryInfo
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {

        log.info("分页查询 --> 页码 --> {},页数大小 --> {}",queryInfo.getPageNumber(),queryInfo.getPageSize());
        PageHelper.startPage(queryInfo.getPageNumber(),queryInfo.getPageSize());
        Page<SysUser> page = sysUserMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        List<SysUser> result = page.getResult();
        result.forEach(item -> {
            item.setRoles(sysUserMapper.findRoles(item.getId()));
            item.setName(item.getUsername());
            item.setPassword(null);
        });
        return PageResult.pageResult(total,result);
    }

    /**
     * 添加用户信息
     *
     * @param user
     * @return
     */
    @Transactional
    @Override
    public Result insert(SysUser user) {
        log.info("根据用户名查询用户信息");
        SysUser sysUser = sysUserMapper.findByUsername(user.getUsername());
        if(null != sysUser){
            return Result.fail("用户名已经存在");
        }
        log.info("给密码加密");
        user.setPassword(passwordEncoder.encode(MD5Utils.md5(user.getPassword())));
        log.info("1.添加用户信息");
        sysUserMapper.insert(user);
        log.info("2. 添加角色信息");
        List<SysRole> roles = user.getRoles();
        if(roles.size() > 0){
            roles.forEach(item -> {
                sysUserMapper.insertUserRoles(user.getId(),item.getId());
            });
        }
        log.info("3.用户的角色添加成功的有{}个",roles.size());
        return Result.success("用户添加成功");
    }

    /**
     * 修改
     *
     * @param user
     * @return
     */
    @Transactional
    @Override
    public Result update(SysUser user) {
        log.info("1.先将用户角色信息删除");
        sysUserMapper.deleteRolesByUserId(user.getId());
        log.info("2.添加用户角色信息");
        List<SysRole> roles = user.getRoles();
        if (roles.size()>0) {
            roles.forEach(item -> {
                sysUserMapper.insertUserRoles(user.getId(),item.getId());
            });
        }
        log.info("给密码加密");
        user.setPassword(passwordEncoder.encode(MD5Utils.md5(user.getPassword())));
        log.info("3.修改用户信息");
        sysUserMapper.update(user);
        return Result.success("用户信息修改成功");
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {
        // 健壮性判断
        SysUser user = sysUserMapper.findById(id);
        if(null == user){
            return Result.fail("用户ID不存在");
        }
        sysUserMapper.deleteRolesByUserId(id);
        sysUserMapper.delete(id);
        return Result.success("用户信息删除成功!");
    }

    /**
     * 根据邮箱修改密码
     *
     * @param email
     * @param password
     */
    @Override
    public void updatePwdByMail(String email, String password) {
        log.info("邮箱修改密码");
        sysUserMapper.updatePwdByMail(email,password);
    }

    /**
     * 微信小程序登录
     *
     * @param openId
     * @return
     */
    @Override
    public Result miniLogin(String openId) {
        UserDetails userDetails;
        userDetails = userDetailsService.loadUserByUsername(openId);
        if(userDetails == null){
            sysUserMapper.insertOpenid(openId);
            userDetails = userDetailsService.loadUserByUsername(openId);
        }

        // 账号 里面 1 表示没有被禁用，0表示被禁用  1 为true ，0为false
        if(!userDetails.isEnabled()){
            return Result.fail("该账号已禁用，请联系管理员");
        }
        log.info("微信小程序登录成功");
        // 用来设置登录信息的token
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
        // 在SpringSecurity中保存登录的 在SpringSecurity中，只要获取到Authentication对象，就可以获取到登录用户的详细信息
        // 设置 Authentication 对象
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        log.info("根据登录信息获取token");
        // 借助JWT生成token
        //需要借助jwt来生成token
        String token = tokenUtils.generateToken(userDetails);
//        String token = "";
//        String token = tokenUtil.generateToken(userDetails);

        Map<String,Object> map = new HashMap<>(4);
        map.put("tokenHead",tokenHead);
        map.put("token",token);
        map.put("userInfo",userDetails);
        map.put("openid",openId);
        return Result.success("登录成功",map);
    }

    /**
     * 根据openId更新用户信息
     *
     * @param user
     */
    @Override
    public Result updateByopenId(SysUser user) {
        if(StringUtils.isEmpty(user.getOpenId())){
            return Result.fail("请传递小程序唯一标识");
        }
        sysUserMapper.updateByopenId(user);
        return Result.success("用户信息更新成功");
    }
}
