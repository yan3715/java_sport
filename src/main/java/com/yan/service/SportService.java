package com.yan.service;

import com.yan.pojo.Sport;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;

public interface SportService {

    /**
     * 删除菜品
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 获取详情
     * @param id
     * @return
     */
    Result findInfo(Long id);

    /**
     * 修改菜品
     * @param sport
     * @return
     */
    Result update(Sport sport);

    /**
     * 添加菜品
     * @param sport
     * @return
     */
    Result insert(Sport sport);

    /**
     * 分页查询菜品信息
     * @param queryInfo
     * @return
     */
    Result findPage(QueryInfo queryInfo);

}
