package com.yan.service;

import com.yan.pojo.SysMenu;
import com.yan.pojo.SysPermission;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;

import java.util.List;

public interface SysMenuService {

    /**
     * 分页查询
     * @param queryInfo 页码，页数大小，查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加权限数据
     * @param menu
     * @return
     */
    Result insert(SysMenu menu);

    /**
     * 删除权限数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改菜单数据
     * @param menu
     * @return
     */
    Result update(SysMenu menu);


    /**
     * 查询所有父级菜单
     * @return
     */
    Result findParent();
}
