package com.yan.service;

import com.yan.pojo.SysMenu;
import com.yan.pojo.SysUser;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;
import com.yan.vo.LoginVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface SysUserService {

    /**
     * 查找所有的用户信息
     * @return
     */
//    Result findAll();

    /**
     *
     * 登录接口
     * @param loginVo 登录参数  账户和密码
     * @return token 用token获取资源
     */
    Result login(LoginVo loginVo);


    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    SysUser findByUsername(String username);


    /**
     * 分页查询 用户信息
     * @param queryInfo
     * @return
     */
    Result findPage(QueryInfo queryInfo);


    /**
     * 添加用户信息
     * @param user
     * @return
     */
    Result insert(SysUser user);


    /**
     * 修改
     * @param user
     * @return
     */
    Result update(SysUser user);

    /**
     * 删除
     * @param id
     * @return
     */
    Result delete(Long id);


    /**
     * 根据邮箱修改密码
     * @param email
     * @param password
     */
    void updatePwdByMail(String email, String password);

    /**
     * 微信小程序登录
     * @param openId
     * @return
     */
    Result miniLogin(String openId);

    /**
     * 根据openId更新用户信息
     * @param user
     */
    Result updateByopenId(SysUser user);
}
