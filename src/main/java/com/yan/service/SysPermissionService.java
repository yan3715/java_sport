package com.yan.service;

import com.yan.pojo.SysPermission;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;

public interface SysPermissionService {

    /**
     * 分页查询
     * @param queryInfo 页码，页数大小，查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加权限数据
     * @param sysPermission
     * @return
     */
    Result insert(SysPermission sysPermission);

    /**
     * 删除权限数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改权限数据
     * @param sysPermission
     * @return
     */
    Result update(SysPermission sysPermission);

    /**
     * 查询所有的权限信息
     * @return
     */
    Result findAll();
}
