package com.yan.service;

import com.yan.pojo.SysMenu;
import com.yan.pojo.SysRole;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;

/**
 * SysRoleService 层
 */
public interface SysRoleService {

    Result findPage(QueryInfo queryInfo);

    Result insert(SysRole role);

    Result delete(Long id);

    Result update(SysRole role);


    Result findAll();
}
