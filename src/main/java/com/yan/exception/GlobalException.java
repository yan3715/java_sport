package com.yan.exception;

import com.yan.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局的异常拦截
 * RestControllerAdvice 是一个符合注解，捕获客户端返回异常案例(自定义返回异常
 * @RestContrllerAdvice是一种组合注解，由@ControllerAdvice，@ResponseBody组成
 * @ControllerAdvice继承了@Component，反过来，可以理解为@RestContrllerAdvice本质上就是@Component
 */
@Slf4j
@RestControllerAdvice
public class GlobalException {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = RuntimeException.class)
    public Result exception(RuntimeException e) {
        e.printStackTrace();
        log.error("系统运行时异常 --> {}",e.getMessage());
        return Result.fail(e.getMessage());
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = AccessDeniedException.class)
    public Result exception(AccessDeniedException e) {
        log.error("权限不足--->{}", e.getMessage());
        return Result.fail("权限不足，请联系管理员！");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = UsernameNotFoundException.class)
    public Result exception(UsernameNotFoundException e){
        log.error("用户名没有找到---{}",e.getMessage());
        return Result.fail(e.getMessage());
    }

}
