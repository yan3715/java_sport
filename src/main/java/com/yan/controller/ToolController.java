package com.yan.controller;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.yan.service.SysUserService;
import com.yan.utils.*;
import com.yan.vo.MailVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 工具控制器
 */

@RestController
@RequestMapping("/tool")
public class ToolController {

    @Autowired
    private QiniuUtils qiniuUtils;

    @Autowired
    private MailUtils mailUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SysUserService userService;

    @Autowired
    private SmsUtils smsUtils;

    @Autowired
    private RedisUtils redisUtils;

   /* @Value("${qiniu.accessKey}")
    private String accessKey;

    @Value("${qiniu.secretKey}")
    private String secretKey;

    @Value("${qiniu.bucket}")
    private String bucket;*/

    @ApiOperation(value = "七牛云文件上传")
    @PostMapping("/upload")
    public Result upload(@RequestBody MultipartFile file) throws IOException {
        String url = qiniuUtils.upload(file.getBytes(), file.getOriginalFilename());
        return Result.success("上传成功",url);
    }

   /* @PostMapping("/upload")
    public Result upload(@RequestBody MultipartFile file) throws IOException {

        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.huanan());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = this.genName(file.getOriginalFilename());
        try {
            byte[] uploadBytes = file.getBytes();
            Auth auth = Auth.create(accessKey, secretKey);
            String upToken = auth.uploadToken(bucket);
            try {
                Response response = uploadManager.put(uploadBytes, key, upToken);
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                System.out.println(putRet.key);
                System.out.println(putRet.hash);
            } catch (QiniuException ex) {
                ex.printStackTrace();
                if (ex.response != null) {
                    System.err.println(ex.response);
                    try {
                        String body = ex.response.toString();
                        System.err.println(body);
                    } catch (Exception ignored) {
                    }
                }
            }
        } catch (UnsupportedEncodingException ex) {
            //ignore
        }

        return Result.success("上传成功");
    }

    public String genName(String fileName){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return format.format(new Date()) + fileName;
    }*/

    @ApiOperation(value = "忘记密码?邮件找回")
    @PostMapping("/forget/password")
    public Result forget(@RequestBody MailVo mail) {
        mail.setSubject("个人运动管理平台");
        Random random = new Random();
        int password = 100000 + random.nextInt(1000000);
        String passwordMD5 = MD5Utils.md5(String.valueOf(password));
        String passwordNew = passwordEncoder.encode(passwordMD5);
        userService.updatePwdByMail(mail.getReceivers()[0],passwordNew);
        mail.setContent("您的新密码: "+ password + ",请妥善保管");

        return Result.success(mailUtils.sendMail(mail));
    }

    @PostMapping("/sms")
    public Result sendSms(String phoneNumber) {
        Random random = new Random();
        int code =  100000 + random.nextInt(899999);
        System.out.println(code);
        smsUtils.sendSms(phoneNumber,code);
        redisUtils.setValueTime(phoneNumber+"sms",code,5);
        return Result.success("验证码发送成功!");
    }
}
