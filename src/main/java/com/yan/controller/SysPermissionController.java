package com.yan.controller;

import com.yan.pojo.SysPermission;
import com.yan.service.SysPermissionService;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 权限数据控制层
 */
@Api(tags = "权限数据")
@RestController
@RequestMapping("/permission")
public class SysPermissionController {

    @Autowired
    private SysPermissionService permissionService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return permissionService.findPage(queryInfo);
    }

    @ApiOperation(value = "添加权限")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysPermission sysPermission){
        return permissionService.insert(sysPermission);
    }

    @ApiOperation(value = "删除权限")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return permissionService.delete(id);
    }

    @ApiOperation(value = "修改权限")
    @PostMapping("/update")
    public Result update(@RequestBody SysPermission sysPermission){
        return permissionService.update(sysPermission);
    }

    @ApiOperation(value = "查询所有的权限")
    @GetMapping("/findAll")
    public Result findAll(){
        return permissionService.findAll();
    }
}
