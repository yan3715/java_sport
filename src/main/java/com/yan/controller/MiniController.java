package com.yan.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yan.pojo.SysUser;
import com.yan.service.SysUserService;
import com.yan.utils.Result;
import com.yan.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.apache.http.util.EntityUtils;
import org.apache.poi.poifs.filesystem.EntryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@Api(tags = "微信小程序登录相关接口")
@RestController
@RequestMapping("/mini")
public class MiniController {

    @Value("${mini.appid}")
    private String appid;

    @Value("${mini.secret}")
    private String secret;


    @Autowired
    private SysUserService userService;

    @GetMapping("/login")
    public Result login(String code) throws IOException {
        if(StringUtils.isEmpty(code)){
            return Result.fail("登录失败，请联系管理员");
        }


        // 创建一个请求对象
//        CloseableHttpClient client = HttpClientBuilder().create().build();
        CloseableHttpClient client = HttpClientBuilder.create().build();

        String url = "https://api.weixin.qq.com/sns/jscode2session?"
        +"appid=" +appid
        +"&secret=" +secret
        +"&js_code=" +code
        +"&grant_type=authorization_code";

        //https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code

        // 构建get请求
        HttpGet get = new HttpGet(url);
        // 发送请求
        CloseableHttpResponse response = client.execute(get);
        log.info("请求响应码:{}", response.getStatusLine().getStatusCode());
        String result = EntityUtils.toString(response.getEntity());
        log.info("请求响应结果：--> {}",result);
        JSONObject jsonObject = JSON.parseObject(result);
        log.info("jsonObject:{}",jsonObject);
        String openid = jsonObject.getString("openid");
        log.info("微信小程序唯一标识:{}",openid);

        Result res = userService.miniLogin(openid);


//        return Result.success("登录成功",res);
        return res;
    }

    @ApiOperation(value = "更新用户信息")
    @PostMapping("/update/info")
    public Result updateInfo(@RequestBody SysUser user){
        return userService.updateByopenId(user);
    }
}
