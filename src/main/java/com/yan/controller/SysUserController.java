package com.yan.controller;

import com.yan.pojo.SysUser;
import com.yan.service.SysUserService;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Autowired
    private SysUserService userService;

    @RequestMapping("/findPage")
    @ApiOperation(value = "分页查询")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return userService.findPage(queryInfo);
    }

    @ApiOperation(value = "添加用户")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysUser user){
        return userService.insert(user);
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改用户信息")
    public Result update(@RequestBody SysUser user){
        return userService.update(user);
    }

    @ApiOperation(value = "删除用户信息")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return userService.delete(id);
    }
}
