package com.yan.controller;

import com.yan.service.SysUserService;
import com.yan.utils.RedisUtils;
import com.yan.utils.Result;
import com.yan.utils.SecurityUtils;
import com.yan.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * 登录
 * 退出
 * 获取当前登录用户的基本信息
 * 相关的接口
 */
@RestController
@RequestMapping("/user")
@Api(value = "用户使用接口")
public class LoginController {

    @Autowired
    private SysUserService userService;

    @Autowired
    private RedisUtils redisUtils;

    @ApiOperation(value = "登录接口")
    @PostMapping("/login")
    public Result login(@RequestBody LoginVo loginVo){
        return userService.login(loginVo);
    }

    @ApiOperation(value = "短信登录")
    @PostMapping("/sms/login")
    public Result smsLogin(@RequestBody LoginVo loginVo){
        return userService.login(loginVo);
    }

/*    @PostMapping("/login")
    public Result login(){
        return userService.findAll();
    }*/

    @ApiOperation(value = "获取用户基本信息")
    @GetMapping("/getInfo")
    public Result getUserInfo(){
        /*if(null == principal){
            return Result.fail("请登录");
        }*/
        return Result.success("获取用户信息成功", SecurityUtils.getUser());
    }

    @ApiOperation(value = "用户退出登录")
    @GetMapping("/logout")
    public Result logout(){
        redisUtils.delKey("userInfo_" + SecurityUtils.getUsername());
        // 获取被记录的登录的信息
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null){
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return Result.success("退出成功,信息清空");
    }
}
