package com.yan.controller;

import com.yan.pojo.SysMenu;
import com.yan.service.SysMenuService;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 菜单信息
 */
@Api(tags = "菜单信息")
@RestController
@RequestMapping("/menu")
public class SysMenuController {
    @Autowired
    private SysMenuService menuService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return menuService.findPage(queryInfo);
    }

    @ApiOperation(value = "添加菜单信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysMenu menu){
        return menuService.insert(menu);
    }

    @ApiOperation(value = "删除菜单信息")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return menuService.delete(id);
    }

    @ApiOperation(value = "修改菜单信息")
    @PostMapping("/update")
    public Result update(@RequestBody SysMenu menu){
        return menuService.update(menu);
    }

    @ApiOperation(value = "查询所有父级菜单")
    @GetMapping("/findParent")
    public Result findParent(){
        return menuService.findParent();
    }
}
