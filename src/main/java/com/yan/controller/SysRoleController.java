package com.yan.controller;

import com.yan.pojo.SysRole;
import com.yan.service.SysRoleService;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "角色数据")
@RestController
@RequestMapping("/role")
public class SysRoleController {

    @Autowired
    private SysRoleService roleService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return roleService.findPage(queryInfo);
    }

    @ApiOperation(value = "删除角色信息")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id) {
        return roleService.delete(id);
    }

    @ApiOperation(value = "添加角色信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysRole role){
        return roleService.insert(role);
    }

    @ApiOperation(value = "修改角色信息")
    @PostMapping("/update")
    public Result update(@RequestBody SysRole role){
        return roleService.update(role);
    }

    @ApiOperation(value = "添加用户时查询所有的角色信息")
    @GetMapping("/findAll")
    public Result findAll(){
        return roleService.findAll();
    }

}
