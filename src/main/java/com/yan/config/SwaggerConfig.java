package com.yan.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableSwagger2 // 开启Swagger2
@EnableKnife4j  // 开启可视化文档
public class SwaggerConfig {

    /**
     * 创建接口文档
     * @return
     */
    @Bean
    public Docket createApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                //useDefaultResponseMessages方法告诉Swagger不使用默认的HTTP响应消息
                //如果没有做自定义响应处理,可以用swagger做自定义响应处理,useDefaultResponseMessages 必须false
                .useDefaultResponseMessages(false)
                .enable(true)
                // 通过apiInfo()属性配置文档信息
                .apiInfo(apiInfo())
                // select():返回 ApiSelectorBuilder 对象，通过对象调用 build()可以创建 Docket 对象  ApiInfoBuilder：ApiInfo 构建器。
                .select()
                // 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
                .apis(RequestHandlerSelectors.basePackage("com.yan.controller"))
                // 配置如何通过path过滤,即这里只扫描请求以/kuang开头的接口
                .paths(PathSelectors.any())
                .build()

                // 方法实现全局token认证
                // 增加Authorization请求头
                // 设置鉴权方式
                .securitySchemes(securitySchemes())
                //设置鉴权范围
                .securityContexts(securityContexts());
    }

    /**
     * 设置文档信息
     * @return
     */
    public ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("个人运动管理平台")
                .version("1.0.0")
                .contact(new Contact("yanlingfei","http://localhost:8090/doc.html","15293842267@163.com"))
                .description("个人运动管理平台的接口文档")
                .build();
    }

    /**
     * 设置请求的信息
     * @return
     */
    public List<ApiKey> securitySchemes() {
        List<ApiKey> list = new ArrayList<>();
        ApiKey key = new ApiKey("Authorization","Authorization","Header");
        list.add(key);
        return list;
    }

    /**
     * 配置security 对 swagger 测试的权限
     * @return
     */
    public List<SecurityContext> securityContexts () {
        List<SecurityContext> list = new ArrayList<>();
        list.add(getSecurityContext());
        return list;
    }

    /**
     * 得到授权路径
     * @return
     */
    private SecurityContext getSecurityContext() {
        return SecurityContext
                .builder()
                .securityReferences(securityReferences())
                .forPaths(PathSelectors.regex("^(?!auth).*$"))
                .build();
    }

    /**
     * 给授权swagger 可以进行接口测试
     * @return
     */
    private List<SecurityReference> securityReferences() {
        List<SecurityReference> list = new ArrayList<>();
        // 授权范围
        AuthorizationScope scope = new AuthorizationScope("global","accessEverything");
        AuthorizationScope[] scopes = new AuthorizationScope[1];
        scopes[0] = scope;
        list.add(new SecurityReference("Authorization",scopes));
        return list;
    }



    /*
        @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).
                useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("^(?!auth).*$"))
                .build()
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }
    private List<SecurityScheme> securitySchemes(){
        return Collections.singletonList(new ApiKey(HttpConstant.AUTHORIZATION, HttpConstant.AUTHORIZATION, "header"));
    }

    private List<SecurityContext> securityContexts() {
        return Collections.singletonList(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .operationSelector(null)
                        .build()
        );
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        return Collections.singletonList(new SecurityReference("Authorization", new AuthorizationScope[]{authorizationScope}));
    }
     */

}
