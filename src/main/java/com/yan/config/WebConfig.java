package com.yan.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
                // 允许访问的路径
                .addMapping("/**")
                // 配置请求来源
                .allowedOrigins("http://localhost:8888")
                // 允许跨域的方法
                .allowedMethods("GET","POST","PUT","DELETE","OPTION")
                // 是否允许携带参数
                .allowCredentials(true)
                // 最大响应时间
                .maxAge(3000)
                .allowedHeaders("*");
    }
}
