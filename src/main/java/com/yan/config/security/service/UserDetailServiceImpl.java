package com.yan.config.security.service;

import com.yan.mapper.SysUserMapper;
import com.yan.pojo.SysMenu;
import com.yan.pojo.SysRole;
import com.yan.pojo.SysUser;
import com.yan.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * 实现自定义登录逻辑
 *


 /**
 * 重写这个方法，从数据库中查找
 * 实现自定义登录逻辑
 * */
@Component
public class UserDetailServiceImpl implements UserDetailsService {

    @Resource
    private SysUserMapper userMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 判断缓存中是否存在用户信息，存在则在缓存中取，不存在则查询数据库并把数据存入缓存
        SysUser user;
        if (redisUtils.haskey("userInfo_" + username)) {
            // 缓存中存在用户信息,直接从redis中取
            user = (SysUser) redisUtils.getValue("userInfo_" + username);
            redisUtils.expire("userInfo_"+username,5);
        } else {
            user = userMapper.findByUsername(username);
            if (null == user) {
                throw new UsernameNotFoundException("用户名或密码错误");
            }
            // 如果当前用户是否是管理员
            if (user.isAdmin()) {
           /* List<SysRole> list = new ArrayList<>();
            SysRole sysRole = new SysRole();
            sysRole.setCode("admin");
            list.add(sysRole);
            user.setRoles(list);
            user.setMenus(userMapper.findMenus(null));*/
                user.setRoles(userMapper.findRoles(null));
                user.setPermissions(userMapper.findPermissions(null));
                // 查询出父级菜单
                List<SysMenu> menus = userMapper.findMenus(null);
                // 获取子级菜单
                menus.forEach(item -> {
                    item.setChildren(userMapper.findChildrenMenu(item.getId(), null));
                });
                user.setMenus(menus);

            } else {

                // 非管理员需要查询角色信息
                List<SysRole> roles = userMapper.findRoles(user.getId());
                user.setPermissions(userMapper.findPermissions(user.getId()));
                user.setRoles(roles);
                // 查询出父级菜单
                List<SysMenu> menus = userMapper.findMenus(user.getId());
                // 获取子级菜单
                menus.forEach(item -> {
                    item.setChildren(userMapper.findChildrenMenu(item.getId(), user.getId()));
                });
                user.setMenus(menus);
            }
            redisUtils.setValueTime("userInfo_"+username,user,5);
        }
//        user.setPassword(null);
        return user;
    }
}
