package com.yan.config.security.contents;

/**
 * 白名单
 */

public class SecurityContents {

    public static final String[] WHITE_LIST = {

            // 后端登录接口
            "/user/login",

            // swagger 相关的
            "/webjars/**",
            "/swagger-ui.html",
            "/doc.html",
            "/swagger-resources/**",
            "/v2/**",
            "/configuration/ui",
            "/configuration/security",
            "/favicon.ico",
            "/tool/forget/password",
            "/tool/sms",
            "/user/sms/login",

            // 小程序相关的
            "/mini/login",
    };
}
