package com.yan.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询数据的封装
 * @param
 */

@EqualsAndHashCode(callSuper = true)  // 消除警告
@Data
public class PageResult extends Result implements Serializable {

    @ApiModelProperty("总记录数")
    // 总记录数
    private long total;

    /**
     * 数据
     */
    @ApiModelProperty(value = "分页的数据列表")
    private List<?> list;

    public PageResult(long total,List<?> list){
        this.setFlag(true);
        this.setMessage("分页查询成功");
        // 总纪录条数
        this.total = total;
        // 总数据
        this.list = list;
    }

    /**
     * 直接返回分页数据
     * @param total 分页总条数
     * @param list 分页数据列表
     * @return
     */
    public static Result pageResult(long total,List<?> list){
        return new PageResult(total,list);
    }


}
