package com.yan.utils;


import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 七牛云文件上传工具类
 */
@Component
@Slf4j
public class QiniuUtils {

    @Value("${qiniu.accessKey}")
    private String accessKey;

    @Value("${qiniu.secretKey}")
    private String secretKey;

    @Value("${qiniu.bucket}")
    private String bucket;


    /**
     * 根据文件路径上传文件
     * @param filePath
     * @param fileName
     * @return
     */
    public String upload(String filePath,String fileName){
        Configuration cfg = new Configuration(Region.region2());
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(accessKey, secretKey);
        String uploadToken = auth.uploadToken(bucket);
        String name = this.genName(fileName);
        try {
            Response response = uploadManager.put(filePath, name, uploadToken);
            return name;
        } catch (QiniuException e) {
            Response r = e.response;
            try{
                log.error("文件上传失败==>{}",r.bodyString());
            }catch (QiniuException ex2){
                // ignore
            }

            return null;
        }
    }


    /**
     * 根据字节上传文件按
     * @param bytes
     * @param fileName
     * @return
     */
    public String upload(byte[] bytes,String fileName) {

        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.huanan());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = this.genName(fileName);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        try {
            Response response = uploadManager.put(bytes, key, upToken);
            log.info("文件上传成功！--> {}",response);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            System.out.println(putRet.key);
            System.out.println(putRet.hash);
            return key;
        } catch (QiniuException ex) {
            ex.printStackTrace();
            if (ex.response != null) {
                System.err.println(ex.response);
                try {
                    String body = ex.response.toString();
                    System.err.println(body);
                } catch (Exception ignored) {
                }
            }
            return null;
        }
    }

    /**
     * 根据文件输入流上传文件
     * @param stream
     * @param fileName
     * @return
     */
    public String upload(InputStream stream, String fileName){

        Configuration cfg = new Configuration(Region.region2());
        UploadManager uploadManager = new UploadManager(cfg);

        //鉴权
        Auth auth = Auth.create(accessKey, secretKey);
        String uploadToken = auth.uploadToken(bucket);
        String name = this.genName(fileName);

        try {
            Response response = uploadManager.put(stream, name, uploadToken,null,null);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            log.info("文件上传成功==>{}",putRet);
            return name;
        } catch (QiniuException e) {
            Response r = e.response;
            try{
//                System.out.println(r.bodyString());
                log.error("文件上传失败==>{}",r.bodyString());
            }catch (QiniuException ex2){
                // ignore
            }

            return null;
        }
    }

    /**
     * 删除 根据文件名进行删除文件
     * @param fileName
     */
    public void delete(String fileName){
        //构造一个指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region1());
        Auth auth = Auth.create(accessKey,secretKey);
        BucketManager bucketManager = new BucketManager(auth,cfg);
        try{
            bucketManager.delete(bucket,fileName);
            log.info("删除文件成功!");
        }catch (QiniuException ex){
            //如果遇到异常，删除失败
            log.error("删除失败==> code {}",ex.code());
            log.error(ex.response.toString());
        }
    }

    /**
     * 根据文件名生成时间文件名
     * @param fileName
     * @return
     */
    public String genName(String fileName){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return format.format(new Date()) + fileName;
    }

}
