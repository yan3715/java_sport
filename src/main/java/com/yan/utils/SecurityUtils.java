package com.yan.utils;

import com.yan.pojo.SysUser;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 用于获取当前用户的基本信息
 */
public class SecurityUtils {

    /*
    getAuthorities方法：获取用户权限
    getCredentials方法：用来获取用户凭证，一般来说就是密码。
    getDetails方法：用来获取用户的详细信息，可能是当前的请求之类。
    getPrincipal方法：用来获取当前用户信息信息，可能是一个用户名，也可能是一个用户对象。
    isAuthenticated方法：当前用户是否认证成功。
     */

    /**
     * 从security主体信息中获取用户信息
     * @return
     */
    public static SysUser getUser(){
        SysUser sysUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        sysUser.setPassword(null);
        sysUser.setName(sysUser.getUsername());

        System.out.println(sysUser);
        return sysUser;
    }

    /**
     * 在security中获取用户名
     * @return
     */
    public static String getUsername() {
        return getUser().getUsername();
    }


}
