package com.yan.utils;

/**
 * 字符串判断
 */
public class StringUtils {

    /**
     * 字符串的判断
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str){
        return str != null && !"".equals(str);
    }

    /**
     * 空的判断
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        return str == null || " ".equals(str);
    }
}
