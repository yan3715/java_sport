package com.yan.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "响应参数")
public class Result implements Serializable {


    @ApiModelProperty(value = "是否成功",dataType = "boolean")
    private boolean flag;

    @ApiModelProperty(value = "响应信息",dataType = "String")
    private String message;

    @ApiModelProperty(value = "响应数据",dataType = "Object")
    private Object data;

    public Result(boolean flag,String message){
        this.message = message;
        this.flag = flag;
    }

    /**
     * 成功的
     * @param message
     * @param data
     * @return
     */
    public static Result success(String message,Object data){
        return new Result(true,message,data);
    }

    /**
     * 退出登录成功传递的信息
     * @param message
     * @return
     */
    public static Result success(String message){
        return new Result(true,message);
    }

    /**
     * 失败的信息
     * @param message
     * @return
     */
    public static Result fail(String message){
        return new Result(false,message);
    }


}
