package com.yan.utils;

import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5Utils加密工具类
 */
@Slf4j
public class MD5Utils {

    /**
     * md5 加密
     * @param password 需要加密的内容
     * @return 返回32位的加密串
     */
    public static String md5(String password) {
        if (StringUtils.isNotEmpty(password)){
            byte[] bytes = null;
            try {
                bytes = MessageDigest.getInstance("md5").digest(password.getBytes());
            } catch (NoSuchAlgorithmException e) {
                log.error("没有md5加密算法");
//                e.printStackTrace();
            }
            // 由md5加密算法得到字节数组转换为16进制数字
            StringBuilder code = new StringBuilder(new BigInteger(1, bytes).toString(16));
            // 保证md5加密后是32位
            for (int i = 0; i < 32 - code.length(); i++) {
                code.insert(0,"0");
            }
            return code.toString();
        }else {
            return null;
        }
    }
}
