package com.yan.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分页前端传入数据的封装
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryInfo {

    // 当前是第几页
    private Integer pageNumber;

    // 一页查询多少条数据
    private Integer pageSize;

    // 查询的关键字
    private String queryString;
}
