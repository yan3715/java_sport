package com.yan.utils;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * 阿里云发送短信的服务
 */
@Slf4j
@Component
public class SmsUtils {

    @Value("${aliyun.accessKey}")
    private String accessKey;

    @Value("${aliyun.secretKey}")
    private String secretKey;

    @Value("${aliyun.signName}")
    private String signName;

    @Value("${aliyun.templateCode}")
    private String templateCode;

//    @Autowired
//    private RedisUtils redisUtils;


    /**
     * 构建发送短信的连接
     * @return
     */

    public Client createClient() throws Exception {
        Config config = new Config()
                .setAccessKeyId(accessKey)
                .setAccessKeySecret(secretKey);

        config.endpoint = "dysmsapi.aliyuncs.com";

        return new Client(config);
    }

    /**
     * 传入电话号码，发送短信
     * @param phoneNumber
     * @return
     */
    public void sendSms(String phoneNumber,int code) {
        try {
//            Random random = new Random();
//            int code =  100000 + random.nextInt(899999);
            Client client = this.createClient();
            SendSmsRequest request = new SendSmsRequest()
                    .setPhoneNumbers(phoneNumber)
                    .setSignName(signName)
                    .setTemplateCode(templateCode)
                    .setTemplateParam("{ code: "+ code + "}");
//            redisUtils.setValueTime("sms",code,5);
            SendSmsResponse response = client.sendSms(request);
//            redisUtils.setValueTime("sms",code,5);
            log.info("短信发送结果 --> {}", response.getBody().code +" --------- " + response.getBody().getMessage());
//            log.info("发送短信成功 --> {}" , code);

        } catch (Exception e) {
            log.error("短信发送失败--> {}",e.getMessage());

        }
    }
}
