package com.yan.mapper;

import com.github.pagehelper.Page;
import com.yan.pojo.SysMenu;
import com.yan.pojo.SysPermission;
import com.yan.pojo.SysRole;
import com.yan.pojo.SysUser;
import com.yan.utils.QueryInfo;
import com.yan.utils.Result;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.security.core.parameters.P;

import java.util.List;

/**
 * 用户相关的操作
 */

public interface SysUserMapper {

    /**
     * 查询用户信息
     * @return
     */
    List<SysUser> findAll();

    SysUser findByUsername(@Param("value") String value);

    /**
     * 根据用户id查询角色信息
     * @param userId
     * @return
     */
    List<SysRole> findRoles(@Param("userId") Long userId);

    /**
     * 根据用户id查询菜单信息
     * @param userId
     * @return
     */
    List<SysMenu> findMenus(@Param("userId") Long userId);


    /**
     * 根据父级ID的用户ID 查询子级菜单
     * @param id 父级id
     * @param userId 用户id
     * @return
     */
    List<SysMenu> findChildrenMenu(@Param("id") Long id, @Param("userId") Long userId);

    /**
     * 根据用户id查询权限数据
     * @param userId
     * @return
     */
    List<SysPermission> findPermissions(@Param("userId") Long userId);


    /**
     * 分页查询用户信息
     * @param queryString 分页查询条件
     * @return 分页数据
     */
    Page<SysUser> findPage(String queryString);


    /**
     * 添加用户信息
     * @param user 用户信息
     */
    void insert(SysUser user);

    /**
     * 修改用户信息
     * @param user
     */
    void update(SysUser user);

    /**
     * 删除用户信息
     * @param id 用户id
     */
    void delete(Long id);

    /**
     * 更据用户信息中的角色列表，去添加用户的角色
     * */
    void insertUserRoles(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 更据用户Id删除角色列表
     * @param userId
     */
    void deleteRolesByUserId(@Param("userId") Long userId);


    /**
     * 更据用户Id查询用户的基本信息
     * @param id
     * @return
     */
    SysUser findById(Long id);

    /**
     * 根据邮件去修改密码
     * @param email
     * @param password
     */
    @Update("update sys_user set `password` = #{password} where email = #{email}")
    void updatePwdByMail(@Param("email") String email,@Param("password") String password);

    /**
     * 微信小程序进入添加信息
     * @param openid 微信小程序唯一标识
     */
    @Insert("insert into sys_user(open_id) values (#{openid})")
    void insertOpenid(@Param("openid") String openid);

    /**
     * 根据openId更新用户信息
     * @param user
     */
    void updateByopenId(SysUser user);
}

