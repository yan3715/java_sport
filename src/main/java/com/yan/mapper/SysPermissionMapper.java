package com.yan.mapper;


import com.github.pagehelper.Page;
import com.yan.pojo.SysPermission;
import com.yan.utils.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 权限数据的增删改查
 */
public interface SysPermissionMapper {

    /**
     * 添加权限信息
     * @param permission 权限数据
     */
    void insert(SysPermission permission);

    /**
     * 修改权限信息
     * @param permission 权限数据
     */
    void update(SysPermission permission);

    /**
     * 删除权限信息
     * @param id 权限id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<SysPermission> findPage(@Param("queryString") String queryString);

    /**
     * 根据角色Id查询权限信息
     * @param roleId
     * @return
     */
    List<SysPermission> findByRoleId(@Param("roleId") Long roleId);


    /**
     * 查询所有的权限数据
     * @return
     */
    List<SysPermission> findAll();
}
