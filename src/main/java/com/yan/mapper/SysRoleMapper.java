package com.yan.mapper;

import com.github.pagehelper.Page;
import com.yan.pojo.SysMenu;
import com.yan.pojo.SysPermission;
import com.yan.pojo.SysRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import javax.management.relation.Role;
import java.util.List;

/**
 * 角色
 */
public interface SysRoleMapper {

    /**
     * 角色添加
     * @param role
     */
    void insert(SysRole role);

    /**
     * 角色删除
     * @param id
     */
    void delete(Long id);

    /**
     * 角色更新
     * @param role
     */
    void update(SysRole role);


    /**
     *  分页查询
     * @param queryString 查询条件
     * @return
     */
    Page<SysRole> findPage(@Param("queryString") String queryString);


    /**
     * 根据角色Id 查询出角色信息
     * @param id
     * @return
     */
    SysRole findById(@Param("id") Long id);

    /**
     * 根据角色Id删除对应的菜单信息
     * @param roleId
     */
    void deleteMenuByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色Id删除对应的权限数据
     * @param roleId
     */
    void deletePermissionByRoleId(@Param("roleId") Long roleId);

    /**
     * 添加角色权限信息
     * @param roleId
     * @param permissionId
     */
    void insertPermissions(@Param("roleId") Long roleId, @Param("permissionId") Long permissionId);

    /**
     * 添加菜单信息
     * @param roleId
     * @param menuId
     */
    void insertMenus(@Param("roleId") Long roleId,@Param("menuId") Long menuId);

    /**
     * 根据角色名称查询是否存在角色信息
     * @param label
     * @return
     */
    SysRole findByLabel(String label);

    /**
     * 查询所有的角色信息
     * @return
     */
    List<SysRole> findAll();
}

