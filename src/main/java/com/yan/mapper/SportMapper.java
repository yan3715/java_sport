package com.yan.mapper;

import com.github.pagehelper.Page;
import com.yan.pojo.Sport;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface SportMapper {
    /**
     * 添加
     * @param sport
     */
    @Insert("insert into sport(title, content, create_time, create_name) values (#{title}, #{content}, #{createTime}, #{createName})")
    void insert(Sport sport);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 删除
     * @param id
     */
    Sport findInfo(Long id);

    /**
     * 修改
     * @param sport
     */
    void update(Sport sport);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<Sport> findPage(String queryString);


}
