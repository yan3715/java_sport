package com.yan.mapper;

import com.github.pagehelper.Page;
import com.yan.pojo.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单信息的增删改查
 */

public interface SysMenuMapper {

    /**
     * 添加菜单信息
     * @param menu
     */
    void insert(SysMenu menu);

    /**
     * 更新菜单信息
     * @param menu
     */
    void update(SysMenu menu);

    /**
     * 删除菜单信息
     * @param id 权限id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<SysMenu> findPage(@Param("queryString") String queryString);

    /**
     * 查询所有父级菜单
     * @return
     */
    List<SysMenu> findParent();

    /**
     * 根据角色Id 查询出菜单信息
     * @param roleId
     * @return
     */
    List<SysMenu> findByRoleId(@Param("roleId") Long roleId);


    /**
     * 根据角色Id和父级ID查询所有的子级菜单
     * @param parentId
     * @param roleId
     * @return
     */
    List<SysMenu> findByRoleIdAndParentId(@Param("parentId") Long parentId,@Param("roleId") Long roleId);


}
