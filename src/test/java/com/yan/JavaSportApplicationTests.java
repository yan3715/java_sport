package com.yan;

import com.yan.utils.MD5Utils;
import com.yan.utils.MailUtils;
import com.yan.utils.SmsUtils;
import com.yan.vo.MailVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class JavaSportApplicationTests {



    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    void contextLoads() {
//
//        String encode = passwordEncoder.encode("123456");
//        System.out.println(encode);
        String encode = passwordEncoder.encode(MD5Utils.md5("123456"));
        System.out.println(encode);
        boolean matches = passwordEncoder.matches(MD5Utils.md5("123456"), encode);
        System.out.println(matches);
    }

    @Autowired
    private MailUtils mailUtils;
    @Test
    public void testMail(){
        MailVo mail = new MailVo();
        mail.setReceivers(new String[] {"2153087993@qq.com"});
        mail.setSubject("个人运动管理平台");
        mail.setContent("邮件发送测试");
        System.out.println(mailUtils.sendMail(mail));
    }

    @Autowired
    private SmsUtils smsUtils;

//    @Test
//    void testSms() {
//        smsUtils.sendSms("15293842267",1111);
//    }
}
